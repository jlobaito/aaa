module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            js: {
                src: [
            'resources/js/*.js', // All JS in the libs folder
            'resources/js/libs/*.js',
            'resources/js/vendor/*.js'
            ],
            dest: '../web/resources/js/production.js',
        },
            css: {
                src: [
            'resources/css/*.css'
            ],
            dest: '../web/resources/css/production.css',
        },

            
    },
    compass: {
        dist: {
          options: {
            sassDir: 'resources/sass',
            cssDir: '../web/resources/css',
          }
        }
      },
    uglify: {
        js: {
            src: '../web/resources/js/production.js',
            dest: '../web/resources/js/production.min.js'
        }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: '../web/resources/img/',
          src: ['*.{png,jpg,gif}'],
          dest: '../web/resources/img/'
        }]
      }
    },
    
    watch: {
        options: {
            livereload: true,
        },
        scripts: {
            files: [
            'resources/js/*.js', 
            'resources/js/libs/*.js',
            'resources/js/vendor/*.js'
            ],
            tasks: ['concat', 'uglify'],
            options: {
                spawn: false,
            },
        } ,
        css: {
            files: ['resources/sass/*.scss'],
            tasks: ['compass'],
            options: {
                spawn: false,
            }
        },
        
          html: {
            files: ['../web/*.html','../web/resources/css/*.css'],
            options: {
                livereload: true
            }
        },
        images: {
            files: ['../web/resources/img/*.{png,jpg,gif}', '../web/resources/img/*.{png,jpg,gif}'],
            tasks: ['imagemin'],
            options: {
              spawn: false,
            }
          }
    }

});

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-compass');
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'uglify', 'imagemin', 'watch','compass']);

};