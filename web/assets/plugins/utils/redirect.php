
// Add URLs and 301s to the array below

$redirect_map = array(
						"/old.html" => "/page-two.html",
						"/old2.url" => "/new2.url"
						);


$key = $_SERVER["REQUEST_URI"];

if(!empty($redirect_map[$key])) $url = $redirect_map[$key];
if(!empty($redirect_map[$key."/"])) $url = $redirect_map[$key."/"];
if(!empty($redirect_map[rtrim($key,"/")])) $url = $redirect_map[rtrim($key,"/")];

if(isset($url)) {
 header("HTTP/1.1 301 Moved Permanently");
 header("Location: $url");
}